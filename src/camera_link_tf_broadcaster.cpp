#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <iostream>

int main(int argc, char** argv){
	ros::init(argc, argv, "camera_link_tf_broadcaster");
	ros::NodeHandle node;


	std::string camera_frame = "camera_link";
	//std::string camera_frame = "camera_color_optical_frame";

	tf::TransformBroadcaster br;
	tf::Transform transform;

	ros::Rate rate(10.0);
	while (node.ok()){
		transform.setOrigin( tf::Vector3(0.0, 0.0, 1.0) );

		tf::Quaternion q;
		q.setRPY(-3.1415/2 - 0.26179938779, 0, 0);
		transform.setRotation(q);

		br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", camera_frame));

		ros::spinOnce();
		rate.sleep();
	}

	return 0;
};
