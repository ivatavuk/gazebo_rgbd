#!/usr/bin/env python
import sys, time
from docutils.nodes import image

import rospy
import roslib
import cv2
import numpy as np

from cv_bridge import CvBridge, CvBridgeError

from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from roslib.rospack import rospack_depends

def camera_callback(data):
	global img
	np_arr = np.fromstring(data.data, np.uint8)
	img = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
	#print img[0][0]


def color_filter(img):

	img = cv2.GaussianBlur(img, (9, 9), 0)

	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

	lower_hue_red_1 = 0
	upper_hue_red_1 = 10
	lower_red_1 = np.array([lower_hue_red_1, 80, 80])
	upper_red_1 = np.array([upper_hue_red_1, 255, 255])

	lower_hue_red_2 = 170
	upper_hue_red_2 = 180
	lower_red_2 = np.array([lower_hue_red_2, 80, 80])
	upper_red_2 = np.array([upper_hue_red_2, 255, 255])

	mask_1 = cv2.inRange(hsv, lower_red_1, upper_red_1)
	mask_2 = cv2.inRange(hsv, lower_red_2, upper_red_2)

	mask = cv2.bitwise_xor(mask_1, mask_2)
	res = cv2.bitwise_and(img, img, mask=mask)


	return res, mask

def main():

	rospy.init_node('color_filter_test', anonymous=True)
	img_subscriber = rospy.Subscriber("/camera/color/image_raw/compressed", CompressedImage, camera_callback,
									  queue_size=1)

	img_publisher = rospy.Publisher("/color_filter/image_raw/compressed", CompressedImage, queue_size=1)
	mask_compressed_publisher = rospy.Publisher("/color_filter/mask/compressed", CompressedImage, queue_size=1)
	mask_raw_publisher = rospy.Publisher("/color_filter/mask", Image, queue_size=1)

	rate = rospy.Rate(30)
	while not rospy.is_shutdown():
		try:

			res, mask = color_filter(img)

			msg = CompressedImage()
			msg.header.stamp = rospy.Time.now()
			msg.format = "jpeg"
			msg.data = np.array(cv2.imencode('.jpg', res)[1]).tostring()
			img_publisher.publish(msg)

			msg = CompressedImage()
			msg.header.stamp = rospy.Time.now()
			msg.format = "jpeg"
			msg.data = np.array(cv2.imencode('.jpg', mask)[1]).tostring()
			mask_compressed_publisher.publish(msg)

			image_message = CvBridge().cv2_to_imgmsg(mask, encoding="passthrough")
			mask_raw_publisher.publish(image_message)

		except:

			print "waiting..."

		rate.sleep()

if __name__ == '__main__':
	main()
